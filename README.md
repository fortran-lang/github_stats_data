# GitHub Stats Data

This repository contains the JSON data files extracted using the
[github_stats](https://gitlab.com/fortran-lang/github_stats) project.
